### Start With
`ssh walkman6@pratham.hack.wheelseye.in -p 222`   

### Level Goal
Password for `walkman7` is inside file `Password.txt` encoded with simple base64.

### Commands Which May Help
base64, cat

### Next level
Goto [Level.7->8](/Levels/Level.7->8.md)
