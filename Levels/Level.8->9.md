### Start With
`ssh walkman8@pratham.hack.wheelseye.in -p 222`   

### Level Goal
Password for `walkman9` is anywhere in the server, owned by user `root` & **group `walkman8`**

### Commands Which May Help
find, ls

### Hint
keep on finding

### Next level
Goto [Level.9->10](/Levels/Level.9->10.md)
