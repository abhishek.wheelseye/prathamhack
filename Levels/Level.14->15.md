### Start With
`ssh walkman14@pratham.hack.wheelseye.in -p 222`   

### Level Goal
Password for `walkman15` is in the response of that placed webservice.

### Commands Which May Help
lsof, curl

### Next level
Goto [Level.15](/Levels/Level.15.md)
