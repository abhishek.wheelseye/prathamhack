### Start With
`ssh walkman7@pratham.hack.wheelseye.in -p 222`   

### Level Goal
Password for `walkman8` is inside file `Password.txt` encoded by `xxd`.

### Commands Which May Help
cat, xxd

### Hint
xxd & encoding decoding.

### Next level
Goto [Level.8->9](/Levels/Level.8->9.md)
